﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Xml;
using System.Threading.Tasks;

namespace Multichoice
{
    class Program
    {
        static void Main(string[] args)
        {
            XMLReader XmlHandler = new XMLReader(); // initiate a new instance of the XMLReader() class

            int currentQuestionNumber = 1;
            int currentScore = 0;

            while (true) 
            {
                Console.WriteLine("Welcome to the MultiChoice Quiz, please select a file to load questions from.");
                bool loopRunning = XmlHandler.setFile();
                while (loopRunning) // while the game loop is running
                {
                    if (currentQuestionNumber > XmlHandler.questionLength())
                    {
                        Console.WriteLine("Tou have won! Your final score was " + currentScore);
                        break;
                    }
                    string currentQuestionNumberString = currentQuestionNumber.ToString();
                    Console.WriteLine("Question " + currentQuestionNumber + " is: " + XmlHandler.getQuestion(currentQuestionNumberString, "questionText") + "\n");
                    Console.WriteLine("1. " + XmlHandler.getQuestion(currentQuestionNumberString, "choice1"));
                    Console.WriteLine("2. " + XmlHandler.getQuestion(currentQuestionNumberString, "choice2"));
                    Console.WriteLine("3. " + XmlHandler.getQuestion(currentQuestionNumberString, "choice3"));
                    Console.WriteLine("4. " + XmlHandler.getQuestion(currentQuestionNumberString, "choice4") + "\n");

                    Console.WriteLine("Please select an answer: \n");
                    string choiceInput = Console.ReadLine();

                    if (choiceInput == XmlHandler.getQuestion(currentQuestionNumberString, "answer"))
                    {
                        currentScore++;
                        Console.WriteLine("\nThat is Correct. Your score is " + currentScore + ". Press any key to continue.\n\n");
                    }
                    else
                    {
                        Console.WriteLine("\nThat is Wrong. Your score is " + currentScore + ". Press any key to continue.\n\n");
                    }

                    Console.ReadKey();

                    currentQuestionNumber++;
                }

                Console.WriteLine("To quit type 'n'. To restart type 'y'.\n");

                if (Console.ReadLine().ToLower() != "y")
                {
                    break;
                }

            }

            Console.WriteLine("\nThank you for playing");
            Console.ReadKey(); // pause the game
        }

    }

    public class XMLReader
    {

        public string filename = null;

        public bool setFile()
        {
            // sets the xml file from the given filename
            bool fileExists = true;

            Console.WriteLine("\nPlease enter a filename to use.");
            filename = Console.ReadLine();
            filename = filename + ".xml";

            // handles IO errors  
            fileExists = File.Exists("../../Data/" + filename);

            if (fileExists == true)
            {
                Console.WriteLine("\nFile set as: " + filename + "\n");
                return fileExists;
            }
            else if (fileExists == false)
            {
                Console.WriteLine("\nError: File not found.\n");
                return fileExists;
            }

            return false;
        }

        public int questionLength()
        {
            // gets the amount of questions in the loaded xml file
            int count = 0;
            
            // Create an XmlReader
            using (XmlReader lengthReader = XmlReader.Create("../../Data/" + filename))
            {
                while (lengthReader.Read())
                {
                    if (lengthReader.Name.Equals("question") && (lengthReader.NodeType == XmlNodeType.Element))
                    {
                        count++;
                    }
                }
                lengthReader.Close();
            }
                
            return count;
        }

        public string getQuestion(string questionNumber, string questionElementName)
        {
            // gets a questions data from the question number and element
            string questionElementData = null;

            // Create an XmlReader
            using (XmlReader xmlDataReader = XmlReader.Create("../../Data/" + filename))
            {
                while (xmlDataReader.Read()) 
                {
                    xmlDataReader.ReadToFollowing("question");
                    xmlDataReader.MoveToFirstAttribute();

                    if (xmlDataReader.Value == questionNumber)
                    {
                        xmlDataReader.ReadToFollowing(questionElementName);
                        if (xmlDataReader.Name == questionElementName)
                        {
                            questionElementData = xmlDataReader.ReadString();
                            // Console.WriteLine("questionElementData set as: " + questionElementData);
                        }
                    }
                }
                xmlDataReader.Close();
            }

            return questionElementData;
        }

        
    }
}

